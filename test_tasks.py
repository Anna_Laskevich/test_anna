def main():

    task_1()
    task_2()
    task_3()
    task_4()
    task_5()
    task_6()

def task_1():
    # 1. Свяжите переменную с любой строкой,
    # состоящей не менее чем из 15 символов.
    # from audioop import reverse

    a = "I am sorry. I am late. I try to catch up."

    # Извлеките из строки первый символ
    a[0]

    # Извлеките из строки последний символ
    a[-1]

    # Извлеките из строки третий с начала
    a[2]

    # Извлеките из строки третий с конца
    a[-3]

    # Измерьте длину вашей строки
    print(len(a))

    # Переверните строку по словам
    words_d = a.split()
    words_d.reverse()
    res = " ".join(words_d)
    print(res)

    # Переверните строку по буквам
    a[::-1]
    print(a[::-1])

    # Первые восемь символов
    print(a[: 8])


def task_2():
    # 2. Свяжите переменную с списком который состоит из элементов от 1 до 10
    list_test = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    # Извлеките из списка элементы стоящие на нечетных позициях
    list_two = list_test[1::2]
    print(list_two)

    # Найдите максимальное число
    print(max(list_test))

    # Найдите минимальное число
    print(min(list_test))

    # Найдите сумму всех чисел заданного списка
    sum = 0
    for i in list_test:
        sum += i

    print(sum)


    # Из списка вида [[1, 2, 3], [4, 5, 6], [7, 8, 9, 10]]. Напечатайте только первые элементы вложенных списков, те 1 4 7
    list_one = [[1, 2, 3], [4, 5, 6], [7, 8, 9, 10]]

    for i in list_one:
            print(i[0], end = ' ')


def task_3():
    # 3. Используя встроенные функции реализуйте:
    # Объединение 2-х кортежей вида: ('a', 'b', 'c', 'd', 'e') и (1, 2, 3, 4, 5) в словарь.
    # Результат работы программы: {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5}
    one = ('a', 'b', 'c', 'd', 'e')
    two = (1, 2, 3, 4, 5)

    res = {one[i]: two[i] for i, _ in enumerate(two)}
    print("Dictionary constructed from tuples : " + str(res))

    # Конвертация элементов списка ['1', '2', '3'] в числа.
    # Результат работы программы: [1, 2, 3]
    a = ['1', '2', '3']
    result = [int(item) for item in a]
    print(result)


def task_4():
    # 4. Напишите программу которая бы позволила удалить любое количество пробелов в строке(строка может быть любой длины):
    # # в начале строки
    # #в конце строки
    line = " Line not a long.   "
    print(line.lstrip())
    print(line.rstrip())

    # в начале и в конце строки
    print(line)
    print(line.strip())


def task_5():
    #5. Напишите функцию, которая принимает строку и возвращает кол-во заглавных и строчных букв
    # 'The quick Brow Fox' =>
    # Upper case characters: 3
    # Lower case сharacters: 12

    line = input("Enter you phrase: ")

    def up_low(string):

        lowercase_letter_count = 0
        uppercase_letter_count = 0

        for letter in string:
            if letter.isupper():
                uppercase_letter_count += 1
            elif letter.islower():
               lowercase_letter_count += 1
        print("Upper case characters: ", uppercase_letter_count)
       print("Lower case сharacters ", lowercase_letter_count)

    up_low(line)


def task_6():
    # 6. Реализуйте класс который умеет работать с файлами (читать из заданного файла, записывать в заданный файл).
    # Расположение файла - текущая рабочая директория.
    myfile = open('test_1.txt', x)
    print(myfile.read())

    # В отдельном модуле программа спрашивает у пользователя,
    # какую операцию он хочет произвести с файлом и выполняет ее.
    # Все операции должны быть информативными для пользователя.

    print("Use only: w, W, r, R, a, A, x, X")
    par = input("What action you want to do with test_1.txt? ")


def file_actions(s):
#    myfile = open('test_1.txt', x)

    if s == "w" or s == "W":
        myfile = open('test_1.txt', 'w')
        print("You could write")

    elif s == "r" or s == "R":
        myfile = open('test_1.txt', 'r')
        print("You could read")
        print(myfile.read())

    elif s == "a" or s == "A":
        myfile = open('test_1.txt', 'a')
        print("You could write at the end")

    elif s == "x" or s == "X":
        myfile = open('test_1.txt', 'x')
        print("You could open")

    else:
        print("Your choice is wrong !")

    file_actions(par)


# Sorry. Did not make it ((


if __name__ == '__main__':
    main()